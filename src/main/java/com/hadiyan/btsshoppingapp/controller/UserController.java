package com.hadiyan.btsshoppingapp.controller;

import com.hadiyan.btsshoppingapp.dto.LoginDto;
import com.hadiyan.btsshoppingapp.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hadiyan.btsshoppingapp.service.UserService;

@RestController
@RequestMapping(path = "api/users")
@Slf4j
@RequiredArgsConstructor
public class UserController {

    private final UserService service;

    @RequestMapping("/hello")
    public String hello(){
        log.info("HENNLLLLOOOOO");
        return "Hello";
    }

    @PostMapping("/signup")
    public Object register(@RequestBody UserDto dto) {
        return service.register(dto);
    }

    @PostMapping("/signin")
    public Object login(@RequestBody LoginDto login) throws Exception {
        return service.login(login);
    }
}
