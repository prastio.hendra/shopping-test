package com.hadiyan.btsshoppingapp.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hadiyan.btsshoppingapp.dto.ShoppingDto;
import com.hadiyan.btsshoppingapp.model.Shopping;
import com.hadiyan.btsshoppingapp.service.ShoppingService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/")
@RequiredArgsConstructor
public class ShoppingController {

    private final ShoppingService service;

    @PostMapping("/shopping")
    public Object save(@RequestBody ShoppingDto dto) {
        return service.save(dto);
    }

    @GetMapping("/shopping")
    public List<Shopping> findAll(){
        return service.findAll();
    }

    @GetMapping("/shopping/{id}")
    public Shopping findById(@PathVariable Long id){
        return service.findById(id);
    }

    @PutMapping("/shopping/{id}")
    public Object update(@PathVariable Long id, @RequestBody ShoppingDto dto) {
        return service.update(id, dto);
    }

    @DeleteMapping("/shopping/{id}")
    public String delete(@PathVariable Long id) {
        return service.delete(id);
    }
}
