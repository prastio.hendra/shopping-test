package com.hadiyan.btsshoppingapp.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthDto {
    private String token;
}
