package com.hadiyan.btsshoppingapp.serviceimpl;

import com.hadiyan.btsshoppingapp.dto.AuthDto;
import com.hadiyan.btsshoppingapp.dto.LoginDto;
import com.hadiyan.btsshoppingapp.dto.UserDto;
import com.hadiyan.btsshoppingapp.model.User;
import com.hadiyan.btsshoppingapp.repository.UserRepository;
import com.hadiyan.btsshoppingapp.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import com.hadiyan.btsshoppingapp.service.UserService;

import javax.transaction.Transactional;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final UserDetailsServiceImpl userDetailsService;
    private final JwtUtil jwtTokenUtil;

    @Override
    public UserDto register(UserDto dto) {
        User user = dto.getUser();
        User savedUser = userRepository.save(user);
        log.info("SAVED USER : {}", savedUser);
        return dto;
    }

    @Override
    public Object login(LoginDto dto) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(dto.getEmail(), dto.getPassword())
            );
            final UserDetails userDetails = userDetailsService
                    .loadUserByUsername(dto.getEmail());

            final String jwt = jwtTokenUtil.generateToken(userDetails);
            AuthDto response = new AuthDto();
            response.setToken(jwt);
            return response;
        }
        catch (BadCredentialsException e) {
            return ("Incorrect email or password");
        }
    }
}
