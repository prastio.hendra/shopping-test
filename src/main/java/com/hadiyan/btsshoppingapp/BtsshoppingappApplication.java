package com.hadiyan.btsshoppingapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.argument.StructuredArguments;

@SpringBootApplication
@Slf4j
public class BtsshoppingappApplication {

    public static void main(String[] args) {
        SpringApplication.run(BtsshoppingappApplication.class, args);
        log.info("Request with kv : ", StructuredArguments.kv("AppName", "SHOPPING-APP"));
    }
}